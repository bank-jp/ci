import csv
import json

SRC_FILE = "ginkositen.utf8.txt"
DST_BANK_FILE = "data/bank.js"
DST_BRANCH_FILE = "data/%s.js"

def main():
    raw_banks, raw_branches = load_csv(SRC_FILE)

    banks = {b[0]:b[3]+"銀行" for b in raw_banks}
    branches = {b[0]:[] for b in raw_banks}
    for branch in raw_branches:
        branches[branch[0]].append({"code":branch[1], "name":branch[3]+"支店"})

    data = []
    for bank_code in banks:
        data.append({
            "code":bank_code,
            "name":banks[bank_code]
        })
    save_json("bank", DST_BANK_FILE, data)
    
    for bank_code in branches:
        data = branches[bank_code]
        save_json("branch", DST_BRANCH_FILE % bank_code, data)

def load_csv(filename):
    banks = []
    branches = []

    with open(filename) as f:
        reader = csv.reader(f)
        for row in reader:
            if row[4] == "1" :
                banks.append(row)
            if row[4] == "2" :
                branches.append(row)
    return banks, branches


def save_json(prefix, filename, data):
    with open(filename, 'w') as f:
        j = json.dumps(data, ensure_ascii=False, separators=(',', ':'))
        f.write("$bankjp_%s(%s)" % (prefix, j))


if __name__ == "__main__":
    main() 